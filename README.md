# 2D-pywt-display
A functions to improve the visibility of different coefficients of the pywt wavelet transforms, by applying normalisation or histogram equalisation to each coefficients matrix of the wavelet transformation.



Coefficient matrices after the normalisation(equa = False)

![IMAGE_DESCRIPTION](https://gitlab.com/mySpecialUsername/2D-pywt-display/-/raw/main/lena_norm.png)


Coefficient matrices after the histogram equalisation with Hsize= 20

![IMAGE_DESCRIPTION](https://gitlab.com/mySpecialUsername/2D-pywt-display/-/raw/main/lena_equa_20.png)

